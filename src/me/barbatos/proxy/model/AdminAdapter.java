package me.barbatos.proxy.model;

import lombok.Getter;

public class AdminAdapter extends AbstractUser {
	
	@Getter private String Usuario;
	@Getter private String Contra;
	@Getter private String nombre;
	private Admin admin;

	public AdminAdapter(String Usuario, String Contra, String nombre) {
		super(Usuario, Contra);
		admin = new Admin(Usuario,Contra,nombre);
		this.Usuario = Usuario;
		this.Contra = Contra;
		this.nombre = nombre;
	}

	@Override
	public String getTipodeStakeholder() {
		return admin.getTipoUsuario();
	}
	
	@Override
	public boolean AutenticaUser(String Usuario, String Contra) {
		return admin.loguear(Usuario, Contra);
	}
	
	
	@Override
	public boolean isAutentica() {
		super.setAutentica(admin.estaAutenticado());
		return admin.estaAutenticado();
	}
}