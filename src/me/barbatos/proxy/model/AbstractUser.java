package me.barbatos.proxy.model;

import lombok.Getter;
import lombok.Setter;

public abstract class AbstractUser {
	
	private String Usuario;
	private String Contra;
	private boolean Autentica;
	
	private String nombreCompleto;
	
	public AbstractUser(String Usuario, String Contra) {
		this.Usuario = Usuario;
		this.Contra = Contra;
		Autentica = false;
	}

	public boolean AutenticaUser(String Usuario, String Contra) {
		if (!this.Usuario.equals(Usuario) || !this.Contra.equals(Contra)) return false;
		Autentica = true;
		return true;
	}
	
	public abstract String getTipodeStakeholder();

	public String getUsuario() {
		return Usuario;
	}

	public void setUsuario(String Usuario) {
		this.Usuario = Usuario;
	}

	public String getContra() {
		return Contra;
	}

	public void setContra(String Contra) {
		this.Contra = Contra;
	}

	public boolean isAutentica() {
		return Autentica;
	}

	public void setAutentica(boolean Autentica) {
		this.Autentica = Autentica;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	@Override
    public String toString() {
        return "AbstractUser [Usuario=" + Usuario + ", Contra=" + Contra + ", Autentica=" + Autentica + ", nombreCompleto=" + nombreCompleto + "]";
    }
}
