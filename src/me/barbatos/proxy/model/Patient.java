package me.barbatos.proxy.model;

import lombok.Getter;

public class Patient extends AbstractUser {
	
	@Getter private String Usuario;
	@Getter private String Contra;
	private final String userType = "paciente";
	@Getter private String nombre;
	
	
	
	public Patient(String Usuario, String Contra, String nombre) {
		super(Usuario, Contra);
		this.Usuario = Usuario;
		this.Contra = Contra;
		this.nombre = nombre;
	}

	@Override
	public String getTipodeStakeholder() {
		return userType;
	}
}
