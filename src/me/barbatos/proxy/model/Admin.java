package me.barbatos.proxy.model;

import lombok.Getter;

public class Admin {
	@Getter private String Usuario;
	@Getter private String Contra;
	private final String userType = "admin";
	@Getter private String nombre;
	private boolean Autentica;

	public Admin(String Usuario, String Contra, String nombre) {
		this.Usuario = Usuario;
		this.Contra = Contra;
		this.nombre = nombre;
	}

	public String getTipoUsuario() {
		return userType;
	}
	
	public boolean loguear(String Usuario, String Contra) {
		if (!this.Usuario.equals(Usuario) || !this.Contra.equals(Contra)) return false;
		Autentica = true;
		return true;
	}
	
	public boolean estaAutenticado() {
		return Autentica;
	}
}
