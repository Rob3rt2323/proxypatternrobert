package me.barbatos.proxy;

import java.util.ArrayList;

import me.barbatos.proxy.model.AbstractUser;
import me.barbatos.proxy.model.AdminAdapter;

public class Proxy implements IFacade {

    private Facade facade;
    private static ArrayList<AbstractUser> usuarios;

    static {
        usuarios = new ArrayList<>();
        AbstractUser admin = new AdminAdapter("admin", "123", "Jairo");
        usuarios.add(admin);
    }

    public Proxy(Facade facade) {
        this.facade = facade;
    }

    @Override
    public boolean ingresar(AbstractUser usuario) {
        for (AbstractUser abstractUser : usuarios) {
            if (abstractUser.getUsuario().equals(usuario.getUsuario()) && abstractUser.getContra().equals(usuario.getContra())) {
                facade.ingresar(usuario);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean registrar(AdminAdapter admin, AbstractUser usuario) {
        for (AbstractUser user : usuarios) {
            if (user.getTipodeStakeholder().equals(admin.getTipodeStakeholder())) {
                usuarios.add(usuario);
                facade.registrar(admin, usuario);
                return true;
            }
        }
        return false;
    }
}
