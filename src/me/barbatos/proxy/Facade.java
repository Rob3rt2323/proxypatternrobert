package me.barbatos.proxy;

import me.barbatos.proxy.model.AbstractUser;
import me.barbatos.proxy.model.AdminAdapter;

public class Facade implements IFacade {

    @Override
    public boolean ingresar(AbstractUser usuario) {
        System.out.println("Ingreso existoso!");
        return true;
    }

    @Override
    public boolean registrar(AdminAdapter admin, AbstractUser usuario) {
        System.out.println("Se registró con éxito!");
        return true;
    }

}
