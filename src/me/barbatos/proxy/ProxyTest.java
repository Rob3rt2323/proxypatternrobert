package me.barbatos.proxy;

import me.barbatos.proxy.model.AbstractUser;
import me.barbatos.proxy.model.AdminAdapter;
import me.barbatos.proxy.model.Doctor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProxyTest {

    @Test
    void ingresarExitoso() {
        Proxy proxy = new Proxy(new Facade());

        AbstractUser user = new AdminAdapter("admin","123","Jairo");

        assertTrue(proxy.ingresar(user));
    }

    @Test
    void ingresarFallido() {
        Proxy proxy = new Proxy(new Facade());

        AbstractUser user = new AdminAdapter("admin","1234","Jairo");

        assertFalse(proxy.ingresar(user));
    }

    @Test
    void ingresarFallido2() {
        Proxy proxy = new Proxy(new Facade());

        AbstractUser user = new AdminAdapter("123","admin","Jairo");

        assertFalse(proxy.ingresar(user));
    }


    @Test
    void registrar() {
        Proxy proxy = new Proxy(new Facade());

        AdminAdapter user = new AdminAdapter("admin","admin","Jairo");

        AbstractUser user2 = new Doctor("Doctor","1234","Camilo Hernandez");

        assertTrue(proxy.registrar(user,user2));
    }
}