package me.barbatos.proxy;

import me.barbatos.proxy.model.AbstractUser;
import me.barbatos.proxy.model.AdminAdapter;

public interface IFacade {

    public boolean ingresar(AbstractUser usuario);
    public boolean registrar(AdminAdapter admin, AbstractUser usuario);
}
